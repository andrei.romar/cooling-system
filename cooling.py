import http.client
import urllib
import time
import RPi.GPIO as GPIO

key = "1NZ9EG58EHJZWXCP"  # Put your API Key here

halfstep_seq = [
  [1,0,0,0],
  [1,1,0,0],
  [0,1,0,0],
  [0,1,1,0],
  [0,0,1,0],
  [0,0,1,1],
  [0,0,0,1],
  [1,0,0,1]
]

GPIO.setmode(GPIO.BOARD)
control_pins = [7,11,13,15]#motor 1 pins
for pin in control_pins:
  GPIO.setup(pin, GPIO.OUT)
  GPIO.output(pin, 0)

control_pins2 = [12,16,18,22]#motor 2 pins
for pin in control_pins2:
  GPIO.setup(pin, GPIO.OUT)
  GPIO.output(pin, 0)

def speed01():
    for halfstep in range(8):
        for pin in range(4):
            GPIO.output(control_pins[pin], halfstep_seq[halfstep][pin])
        time.sleep(0.02)

def speed02():
    for halfstep in range(8):
        for pin in range(4):
            GPIO.output(control_pins[pin], halfstep_seq[halfstep][pin])
            GPIO.output(control_pins2[pin], halfstep_seq[halfstep][pin])
        time.sleep(0.01)
        
def speed03():
    for halfstep in range(8):
        for pin in range(4):
            GPIO.output(control_pins[pin], halfstep_seq[halfstep][pin])
            GPIO.output(control_pins2[pin], halfstep_seq[halfstep][pin])
        time.sleep(0.005)

def speed04():
    for halfstep in range(8):
        for pin in range(4):
            GPIO.output(control_pins[pin], halfstep_seq[halfstep][pin])
            GPIO.output(control_pins2[pin], halfstep_seq[halfstep][pin])
        time.sleep(0.001)

def thermometer():
    while True:
        #Calculate CPU temperature of Raspberry Pi in Degrees C
        temp = int(open('/sys/class/thermal/thermal_zone0/temp').read()) / 1e3 # Get Pi CPU temp
        params = urllib.parse.urlencode({'field1': temp, 'key':key }) 
        headers = {"Content-typZZe": "application/x-www-form-urlencoded","Accept": "text/plain"}
        conn = http.client.HTTPConnection("api.thingspeak.com:80")
        try:
            conn.request("POST", "/update", params, headers)
            response = conn.getresponse()
            print (temp)
            data = response.read()
            conn.close()
            if temp < 40:
                print ("Rest")
            elif temp <55:
                speed01()
                print("low")
            elif temp <70:
                speed02()
                print("mid")
            elif  temp <85:
                speed03()
                print("high")
            else:
                speed04()
                print("danger")
        except:
            print ("connection failed")
        break

if __name__ == "__main__":
        while True:
                thermometer()
