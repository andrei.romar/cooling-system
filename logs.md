20.04
Project discussed with supervisor.

21.04
Expected plan is made

22.04
Search for information.

23.04
Due to lack of information going back to my old projects.

24.04
Changing project plan. Now instead of building basic system first goal is to combinate old projects.

25.04
Recreation of the project from 1st semester to get access to thingspeak. https://gitlab.com/20a-itt1-project/team_a3

26.04
Recreation of the motors project in the past semester.

27.04
Attempt to combinate them.
Failure.

28.04
Continuing attempts to combinate motors with temperature sensor.

29.04
After >48h working straight with combining them and making any possible changes, including adding batteries, changing wires and totally rewriting code several times got to conclusion to recreate again project from 1st semester.
Figured out that the temperature sensor burned and that's the reason why all didn't work.

2.05
Got sick. Very heavily

10.05
Finally started to feel better, got back to work.
After checking notes and current progress, decided to order new sensor and wait for now.

16.05
Have to wait tomorrow to get sensor from the post distributing shop.

17.05
Spent the whole day trying to make sensor work.
After investigation find out that this sensor is defective.
Asked for another one.

19.05
Spend whole day arguing with the seller.
Will be able to get new sensor no earlier than 1.5-2 weeks.

20.05
Due to hand in date, decided to search for another variant.
After some search found great guide for gathering CPU temperature and sending it to thingspeak.
Decided to immitate PC's temperature on a raspberry pi CPU temperature, so removing temperature sensor from the overall build.

21-23.05
Even though the guide seemed good. It's too old and multiple things weren't working properly. Had to fix all mistakes, but managed to make it work.
Combined motors and cpu codes.
Rewrote motors code from fixed to elif conditions.

24.05
Finished the project.

25.05
Start writing the final project report.

30.05 Current report was shown to supervisor.

31.05 Changes to report that supervisor recommended.

01.06 Finishing conclusion/discussion parts of the report.
