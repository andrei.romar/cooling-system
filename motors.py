import RPi.GPIO as GPIO
import time

#time.sleep = 0.001 # max speed
halfstep_seq = [
  [1,0,0,0],
  [1,1,0,0],
  [0,1,0,0],
  [0,1,1,0],
  [0,0,1,0],
  [0,0,1,1],
  [0,0,0,1],
  [1,0,0,1]
]

GPIO.setmode(GPIO.BOARD)
control_pins = [7,11,13,15]#motor 1 pins
for pin in control_pins:
  GPIO.setup(pin, GPIO.OUT)
  GPIO.output(pin, 0)

control_pins2 = [12,16,18,22]#motor 2 pins
for pin in control_pins2:
  GPIO.setup(pin, GPIO.OUT)
  GPIO.output(pin, 0)

def speed01():
    for halfstep in range(8):
        for pin in range(4):
            GPIO.output(control_pins[pin], halfstep_seq[halfstep][pin])
        time.sleep(0.02)

def speed02():
    for halfstep in range(8):
        for pin in range(4):
            GPIO.output(control_pins[pin], halfstep_seq[halfstep][pin])
            GPIO.output(control_pins2[pin], halfstep_seq[halfstep][pin])
        time.sleep(0.01)
        
def speed03():
    for halfstep in range(8):
        for pin in range(4):
            GPIO.output(control_pins[pin], halfstep_seq[halfstep][pin])
            GPIO.output(control_pins2[pin], halfstep_seq[halfstep][pin])
        time.sleep(0.005)

def speed04():
    for halfstep in range(8):
        for pin in range(4):
            GPIO.output(control_pins[pin], halfstep_seq[halfstep][pin])
            GPIO.output(control_pins2[pin], halfstep_seq[halfstep][pin])
        time.sleep(0.001)

#def base1():
#    for halfstep in range(8):
#        for pin in range(4):
#            GPIO.output(control_pins[pin], halfstep_seq[halfstep][pin])
#        time.sleep(delay)

#def base2():
#    for halfstep in reversed(range(8)):
#        for pin in range(4):
#            GPIO.output(control_pins2[pin], halfstep_seq[halfstep][pin])
#        time.sleep(delay)

try:
    while True:
        for j in range(5):#do 5 times
            print ("1")
            for i in range(50):
                speed01() 
            print ("2")
            for i in range(50):
                speed02() 
            print ("3")
            for i in range(50):
                speed03() 
        print("4")
        for i in range(500):
            speed04()
except KeyboardInterrupt:
    print("Cancelled by user")
except:
    print("Error")
finally:
    GPIO.cleanup()
